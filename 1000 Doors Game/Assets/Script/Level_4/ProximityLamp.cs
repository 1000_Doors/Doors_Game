﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProximityLamp : MonoBehaviour {

    public Light lt;
    public GameObject key;
    public Text lampu;

	void Start () {
        PAProximity.onProximityChange += OnProximityChange;
        OnProximityChange(PAProximity.proximity);

        //lamp
        lt = GetComponent<Light>();
        key.SetActive(false);
	}

    void Update() 
    {
        lampu.text = lt.intensity.ToString();
        visibleKey();
    }

    void OnProximityChange(PAProximity.Proximity proximity) 
    {
        lt.intensity = ((proximity == PAProximity.Proximity.NEAR) ? 0.0f : 4.0f);
    }

    void visibleKey() 
    {
        if (lt.intensity == 0.0f)
        {
            key.SetActive(true);
        }
        else if (lt.intensity == 4.0f)
        {
            key.SetActive(false);
        }
    }
}
