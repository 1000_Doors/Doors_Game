﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObj : MonoBehaviour {

    private Vector3 screenPoint;
    private Vector3 offset;
    //private float firstY;

    void OnMouseDown() 
    {
        //firstY = transform.position.y;
        screenPoint = Camera.main.ScreenToWorldPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(-Input.mousePosition.x, 0, screenPoint.z));
    }

    void OnMouseDrag() 
    {
        Vector3 curScreenPoint = new Vector3(-Input.mousePosition.x,0, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }
}
