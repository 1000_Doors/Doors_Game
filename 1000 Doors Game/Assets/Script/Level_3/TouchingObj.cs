﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TouchingObj : MonoBehaviour {
    public static bool Active;
    public Light lg;

    void Start() 
    {
        Active = false;
        lg.intensity = 0;
    }
    
    void OnMouseDown() 
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag == "Key" && Active == false) 
            {
                lg.intensity = 5;
                Active = true;
            }

            else if (hit.transform.tag == "Key" && Active == true)
            {
                lg.intensity = 0;
                Active = false;
            }
        }
    }
}
