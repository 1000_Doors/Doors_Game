﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour {
    public string level_name;

    public void level() 
    {
        SceneManager.LoadScene(level_name, LoadSceneMode.Single);
    }
}
