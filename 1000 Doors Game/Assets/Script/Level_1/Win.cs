﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour {
    public Text timerText;
    public float sisaWaktu;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        sisaWaktu -= Time.deltaTime;

        timerText.text = sisaWaktu.ToString();

        if(sisaWaktu < 0)
        {
            Debug.Log("Waktu sudah habis gan");
            SceneManager.LoadScene("Level_1");
        }
	}

    void cekMenang() 
    {
    }
}
