﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelero_lvl1 : MonoBehaviour {

    private Rigidbody rigid;
    bool isFlat = true;
    private void Start() 
    {
        rigid = GetComponent<Rigidbody>();
    }

    private void Update() 
    {
        Vector3 tilt = Input.acceleration * 10;

        if (isFlat)
            tilt = Quaternion.Euler(90, 0, 0) * tilt;

        rigid.AddForce(tilt);
    }
}
