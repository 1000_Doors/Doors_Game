﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TapMoveScene : MonoBehaviour {
    public string SceneName;

    void Update() 
    {
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
        }
    }    
}
